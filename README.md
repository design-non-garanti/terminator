# Terminator

## Description

A terms killer that will search for rhetoric in web pages, usable via an innovative bookmark technology.

- `replace1.html`: first version using innerHTML and breaking the DOM (buggy on the original page)
- `replace2.html`: a library dependant version using findAndReplaceDOMText.js
- `replace3.html`: first nicely working version using childNodes
- `replace4.html`:
